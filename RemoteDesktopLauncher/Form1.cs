﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteDesktopLauncher
{
    public partial class Form1 : Form
    {
        #region "controls"
    
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetServerList();
        }

        private void txtServerName_GotFocus(object sender, EventArgs e)
        {
            txtServerName.Text = "";
        }

        private void btnLaunch_Click(object sender, EventArgs e)
        {
            LaunchRdpSession(txtServerName.Text);

        }

        private void lstServers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lstServers.SelectedItems.Count == 0)
                {
                    return;
                }
                //ListViewItem item = lstServers.SelectedItems[lstServers.FocusedItem.Index];
                ListViewItem item = lstServers.SelectedItems[0];
                txtServerName.Text = item.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.ToString());
            }

        }

        private void ListView_MouseDoubleClick(object sender, EventArgs e)
        {
            LaunchRdpSession(txtServerName.Text);
        }
        #endregion

        #region "methods"
        private void GetServerList()
        {
            int counter = 0;
            string line;
            lstServers.Clear();

            try
            {
                System.IO.StreamReader file = new System.IO.StreamReader("servers.txt");
                while ((line = file.ReadLine()) != null)
                {
                    lstServers.Items.Add(line);
                    counter++;
                }

                file.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.ToString());
            }

        }

        private void SaveServerToList()
        {
            string serverName = txtServerName.Text;
            try
            {
                if (!CheckForDuplicateServer(serverName))
                {
                    string path = "servers.txt";

                    if (!File.Exists(path))
                    {
                        using (StreamWriter sw = File.CreateText(path))
                        {
                            sw.WriteLine(serverName);
                        }
                    }
                    else
                    {
                        using (StreamWriter sw = File.AppendText(path))
                        {
                            sw.Write(serverName);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Server already exists in list.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.ToString());
            }


            GetServerList();
        }

        private bool CheckForDuplicateServer(string serverName)
        {

            foreach (ListViewItem item in lstServers.Items)
            {
                if (item.Text == serverName)
                {
                    return true;
                }
            }
            return false;
        }

        private void LaunchRdpSession(string ServerName)
        {
            try
            {
                if (chkSaveServer.Checked)
                {
                    SaveServerToList();
                }
                Process rdcProcess = new Process();
                rdcProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(@"%SystemRoot%\system32\cmdkey.exe");
                rdcProcess.StartInfo.Arguments = "/generic:TERMSRV/" + ServerName + " / user:" + txtUserName.Text + " /pass:" + txtPassword.Text;
                rdcProcess.Start();

                rdcProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(@"%SystemRoot%\system32\mstsc.exe");
                rdcProcess.StartInfo.Arguments = "/v " + ServerName; // ip or name of computer to connect
                rdcProcess.Start();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.ToString());
            }
        }

        #endregion

    }
}
